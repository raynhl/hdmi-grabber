#include <v4l2_device.hpp>

V4l2Device::V4l2Device(std::string path)
{
    this->path = path;

    if (!(this->open_device() && this->support_video_streaming()))
    {
        this->close_device();
        exit(1);
    }
};

V4l2Device::~V4l2Device()
{
    this->close_device();
};

bool V4l2Device::support_video_streaming()
{
    struct v4l2_capability caps;
    if (ioctl(this->fd, VIDIOC_QUERYCAP, &caps) < 0)
    {
        std::cerr << "[ERROR] VIDIOC_QUERYCAP" << std::endl;
        return false;
    }

    if (!(caps.capabilities & V4L2_CAP_VIDEO_CAPTURE))
    {
        std::cerr << "[ERROR] Device does not support streaming video capture." << std::endl;
        return false;
    }

    return true;
}

bool V4l2Device::is_open()
{
    return this->device_open;
}

bool V4l2Device::open_device()
{
    if (this->is_open())
    {
        std::cout << "[WARNING] Device is already open." << std::endl;
        return true;
    }

    if ((this->fd = open(this->path.c_str(), O_RDWR)) < 0)
    {
        std::cout << "[ERROR] Unable to open device: " << this->path << std::endl;
        this->device_open = false;
        return false;
    }

    this->device_open = true;

    std::cout << "[INFO] Device opened: " << this->path << std::endl;
    return true;
}

bool V4l2Device::close_device()
{
    close(this->fd);
    this->device_open = false;
    std::cout << "[INFO] Device closed: " << this->path << std::endl;
    return true;
}

void V4l2Device::print()
{
    std::cout << "[INFO] Device path: " << this->path << std::endl;
};

