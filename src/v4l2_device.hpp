#include <iostream>
#include <string>
#include <stdio.h>
#include <fcntl.h> /* low-level i/o */
#include <unistd.h>
#include <linux/videodev2.h>
#include <sys/ioctl.h>

class V4l2Device
{
    
    public:
        V4l2Device(std::string path);
        ~V4l2Device();

        bool is_open();
        void print();

    private:
        bool open_device();
        bool close_device();
        bool support_video_streaming();

        std::string path;
        int fd = -1;
        bool device_open = false;
};